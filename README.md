RiftValley_WithinHost
====================

This repository contains code used in the following paper.

Cecilia H, Vriens R, Wichgers Schreur PJ, de Wit MM, Métras R, Ezanno P, ten Bosch QA (2022) **Heterogeneity of Rift Valley fever virus transmission potential across livestock hosts, quantified through a model-based analysis of host viral load and vector infection.** *PLoS Comput Biol* 18(7): e1010314. https://doi.org/10.1371/journal.pcbi.1010314

All code contained within this repository is released under the [CRAPL v0.1 License](http://matt.might.net/articles/crapl/). Data provided in this repository are sufficient to rerun all analyses.

The scripts for simulations and model fitting were performed on the INRAE MIGALE bioinformatics facility (doi: 10.15454/1.5572390655343293E12) and on a desktop computer (Ubuntu 18.04.6). Processing of outputs was also done on a desktop computer (Ubuntu 18.04.6) 

====================

### Set up of code base: 

* `Code` contains code to analyze the data, perform calculations, and generate output and figures 
* `Data` contains experimental data described in manuscript. Read by scripts from the `Code` folder
* `Output` contains results generated by scripts in the `Code` folder. 

### Code folder
* `Rift_WithinHost_Functions_Build_BasicModel.R` contains the within-host model solver, implemented with *odin* package to run faster
* `Rift_WithinHost_Functions_Likelihood.R`  contains functions to compute the log-likelihood and the DIC
* `Rift_WithinHost_Functions_MCMC.R` contains functions used to run the MCMC : includes the computation of prior, the optimization of sampling for each parameter, and the MCMC in itself
* `Rift_WithinHost_Fit_and_Metrics_Figure.R` creates figures of within-host model fit to data (Figure 2 preprint version) and outcome measures estimates (R0 and Tg, Figure 3)
* `Revisions_New_Computations.R` contains additions made during the revision process : correlation plot for Supplementary Information, some summary statistics about the experimental data, and a new way to compute uncertainty around within-host model fit (Figure 2)
* `Rift_WithinHost_<GROUP>_Fitting.R` runs the MCMC for each group : optimize standard deviation of sampling, select fixed parameter (initial number of target cells), runs 3 chains with different starting conditions and assessed common convergence
* `Rift_WithinHost_Species_Compare.R` plots profile likelihood to choose target cell initial value, concludes on the difference between groups by computing DIC, plots posterior distributions and outcome measures (Figure 3)
* `Rift_DoseResp_Functions.R` contains the different functional forms used to fit dose-response curves
* `Rift_DoseResp_Main.R` selects the best suited functional form for dose-response curves, concludes on the difference between Aedes and Culex, saves results and plots associated figures (Figure 4).
* `Rift_ViremiaToInfectiousness_Host_Species_Vector_Genus.R` computes and plots the net infectiousness (NI) of groups (Figure 5), combining uncertainty from within-host model fitting and estimation of dose-response curves, computes the survival model for lambs and tests the sensitivity of lambs NI to expected mortality rates.

### Data folder
#### doseResponse subfolder
* `systematic_review_screening_Sept2020.ods` contains the list of papers screened in the systematic review, and the reference numbers further used in `data_Aedes_Culex_Hamster_ZH501.csv`
* `data_Aedes_Culex_Hamster_ZH501.csv` contains data selected for the quantitative analysis of dose-response curves in vectors

#### speciesData subfolder
* `species_data.csv` contains time-series of viral load data for each animal, from Wichgers Schreur et al. 2021 [1]
* `lamb_dead_edited.csv` contains the data on euthanized lambs with their time of death

### Output folder 
* `doseResponse` subfolder contains dose-response curves generated by `Rift_DoseResp_Main.R` (for Aedes, Culex, and both aggregated)
* `speciesFitting` subfolder contains outputs of the MCMC for each group, generated by `Rift_WithinHost_<GROUP>_Fitting.R` (each chains separately and joint posteriors)
* `global_params.RData` contains parameters that need to be used in virtually every script, namely the limit of detections of RNA and TCID50

Any questions about this code base can be directed to quirine.tenbosch@wur.nl or helene.cecilia3@gmail.com

[1] Wichgers Schreur, P.J., Vloet, R.P.M., Kant, J., van Keulen, L., Gonzales, J.L., Visser, T.M., Koenraadt, C.J.M., Vogels, C.B.F., Kortekaas, J., 2021. Reproducing the Rift Valley fever virus mosquito-lamb-mosquito transmission cycle. Sci Rep 11, 1477. https://doi.org/10.1038/s41598-020-79267-1
