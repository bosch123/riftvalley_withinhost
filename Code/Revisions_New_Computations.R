## ---------------------------
##
## Script name: 
##
## Purpose of script:
##
## Author: Helene Cecilia
##
## Date Created: 2022-02-28

rm(list=ls())

## Loading Packages  ------------------
library(corrplot)
library(BayesianTools)
library(dplyr)
library(plyr)
library(patchwork)

## Set Work Directory ------------------------------------------------------
setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) # set to source file location 
getwd()

## Load source files ------------------
source('Rift_WithinHost_Functions_Build_BasicModel.R')
source("Rift_WithinHost_Functions_MCMC.R")
load("../Output/global_params.RData")

## -------------------------------

res.goat <- read.csv("../Output/speciesFitting/joint_goat.csv")
res.calve <- read.csv("../Output/speciesFitting/joint_calve.csv")
res.lamb.surv <- read.csv("../Output/speciesFitting/joint_lamb_surv.csv")
res.lamb.dead <- read.csv("../Output/speciesFitting/joint_lamb_dead.csv")

nbIter <- 100000 # 3 chains, each iter nb is present three times 
burnIn <- 20000
parnames <- c("delta", "p", "xi", "dinf", "ch")

# Pairwise plots of posterior draws -----
res.goat <- res.goat[res.goat$iter>burnIn,]
res.calve <- res.calve[res.calve$iter>burnIn,]
res.lamb.surv <- res.lamb.surv[res.lamb.surv$iter>burnIn,]
res.lamb.dead <- res.lamb.dead[res.lamb.dead$iter>burnIn,]

# 1st solution : plot with points, coefficients and significance test (long)
# panel.cor_simple <- function(x, y, digits=2, prefix="", cex.cor = 20) 
# {
#   usr <- par("usr"); on.exit(par(usr)) 
#   par(usr = c(0, 1, 0, 1)) 
#   r <- cor(x, y) 
#   txt <- format(c(r, 0.123456789), digits=digits)[1] 
#   txt <- paste(prefix, txt, sep="") 
#   if(missing(cex.cor)) cex <- 0.8/strwidth(txt) else cex <- cex.cor
#   
#   test <- cor.test(x,y) 
#   # borrowed from printCoefmat
#   Signif <- symnum(test$p.value, corr = FALSE, na = FALSE, 
#                    cutpoints = c(0, 0.001, 0.01, 0.05, 0.1, 1),
#                    symbols = c("***", "**", "*", ".", " ")) 
#   
#   text(0.5, 0.5, txt, cex = cex * abs(r)) 
#   text(.8, .8, Signif, cex=cex, col=2) 
# } 
# 
# pairs(res.goat[,-c(1,2)], lower.panel=panel.smooth, upper.panel=panel.cor_simple, main = "Goat") 

# 2nd solution : 
# col <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
# M <- cor(res.goat[,-c(1,2)])
# corrplot(M, method="color", col=col(200), 
#          type="upper",
#          addCoef.col = "black")
# 
# M <- cor(res.calve[,-c(1,2)])
# corrplot(M, method="color", col=col(200), 
#          type="upper",
#          addCoef.col = "black")
# 
# M <- cor(res.lamb.surv[,-c(1,2)])
# corrplot(M, method="color", col=col(200), 
#          type="upper",
#          addCoef.col = "black")
# 
# M <- cor(res.lamb.dead[,-c(1,2)])
# corrplot(M, method="color", col=col(200), 
#          type="upper",
#          addCoef.col = "black")

# 3rd solution :
png(filename = "../Output/figures/suppl/revision_goat_correlation_plot.png", width = 700, height = 600)
correlationPlot(res.goat[,-c(1,2)], scaleCorText = FALSE)
dev.off()

png(filename = "../Output/figures/suppl/revision_calve_correlation_plot.png", width = 700, height = 600)
correlationPlot(res.calve[,-c(1,2)], scaleCorText = FALSE)
dev.off()

png(filename = "../Output/figures/suppl/revision_lamb_surv_correlation_plot.png", width = 700, height = 600)
correlationPlot(res.lamb.surv[,-c(1,2)], scaleCorText = FALSE)
dev.off()

png(filename = "../Output/figures/suppl/revision_lamb_dead_correlation_plot.png", width = 700, height = 600)
correlationPlot(res.lamb.dead[,-c(1,2)], scaleCorText = FALSE)
dev.off()

# Summary statistics of raw data -----
group <- c(rep("goat",8),rep("calf",8),rep("lamb",16))
detect_rna <- c(6,5,10,9,7,7,6,5,8,8,6,5,9,4,5,9,4,8,7,3,9,7,6,5,7,3,6,7,5,5,3,4)
detect_tcid <- c(2,3,3,5,3,2,1,2,3,3,3,3,3,2,2,2,3,2,5,2,4,6,5,4,5,2,4,3,3,5,2,2)
max_rna <- c(7.94,8.5,7.98,9.81,8.14,7.37,7.43,8.66,8.73,8.4,8.05,8.09,9.04,8.77,7.79,7.86,
             9.91,8.99,9.71,9.48,9.31,9.8,9.56,9.77,9.54,9.63,9.35,9.18,8.89,9.82,7.45,8.46)
day_max_rna <- c(2,2,2,3,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3)
max_tcid <- c(3.88,4.35,4.12,6.12,4.58,2.49,3.42,3.88,4.82,4.35,4.12,4.58,5.51,4.12,4.35,3.8,
              6.45,5.75,6.45,6.21,5.51,7.15,6.68,7.15,6.41,5.82,6.02,5.62,5.42,6.22,5.01,4.81)
day_max_tcid <- c(2,2,2,2,2,2,2,2,3,3,3,3,3,2,3,3,2,3,3,3,2,3,2,3,3,2,3,2,3,3,3,3)

summary(max_rna)
summary(max_tcid)
summary(day_max_rna)
summary(day_max_tcid)

# Prediction interval around within-host model fit -----

data <- read.csv("../Data/speciesData/species_data.csv")
names(data) <- c("xvals", "outcomeRNA", "outcomeTCID", "host_type", "outcome_survival", "id", "study")
data <- data[!(is.na(data$outcomeRNA) & is.na(data$outcomeTCID)),]

data$inoculum <- NA
data[which(data$host_type == "lamb"),]$inoculum <- 52.6
data[which(data$host_type == "calve"),]$inoculum <- 12.5
data[which(data$host_type == "goat"),]$inoculum <- 62.5

data.all <- data
data.all.surv <- data[data$outcome_survival == "surv",]
data.lamb.all <- data[data$host_type == "lamb",]
data.lamb.surv <- data[data$host_type == "lamb" & data$outcome_survival == "surv",]
# data.lamb.dead <- data[data$host_type == "lamb" & data$outcome_survival == "die",]
# data_lamb_dead <- data.lamb.dead
# data_edit(data_lamb_dead,
#           save_as = "../Data/speciesData/lamb_dead_edited.csv",
#           write_fun = "write.csv",
#           write_args = list(row.names = F)) # add time_of_death boolean for plotting
data.lamb.dead <- read.csv("../Data/speciesData/lamb_dead_edited.csv")
data.goat <- data[data$host_type == "goat",]
data.calve <- data[data$host_type == "calve",]

target <- seq(4,6.5,0.5)

init.state.goat <- c(Vinf0 = 62.5, kl = 3, U0 = 10**target[1], beta = 48/(10**target[1]))
init.state.calve <- c(Vinf0 = 12.5, kl = 3, U0 = 10**target[2], beta = 48/(10**target[2]))
init.state.lamb.surv <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[4], beta = 48/(10**target[4]))
init.state.lamb.dead <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[5], beta = 48/(10**target[5]))

nbIter <- 100000
burnIn <- 20000
parnames <- c("delta", "p", "xi", "dinf", "ch")

# res.goat <- read.csv("../Output/speciesFitting/joint_goat.csv")
# res.calve <- read.csv("../Output/speciesFitting/joint_calve.csv")
# res.lamb.surv <- read.csv("../Output/speciesFitting/joint_lamb_surv.csv")
# res.lamb.dead <- read.csv("../Output/speciesFitting/joint_lamb_dead.csv")

compute_fit_and_uncertainty <- function(results_mcmc, data, burnIn, model.solve.ode, init.state, n_traj){
  tt <- seq(0, max(data$xvals), 0.1)
  
  if("iter" %in% colnames(results_mcmc)){ # for joint posteriors (different formatting)
    res_post_burn_in <- results_mcmc[results_mcmc$iter>burnIn,]
    posterior.param <- subset(res_post_burn_in, select = -c(logLik, iter))
  }else{
    posterior.param <- tail(results_mcmc$parameters,-burnIn)
  }
  s <- sample(nrow(posterior.param), size = n_traj, replace = FALSE)
  env <- NULL
  for(i in s){
    param <- posterior.param[i,]
    names(param) <- parnames
    allpars <- c(init.state, param)

    model.solve.ode$set_user(U0 = allpars[["U0"]], Vinf0 = allpars[["Vinf0"]], 
                             beta = allpars[["beta"]], kl = allpars[["kl"]], delta = allpars[["delta"]],
                             p = allpars[["p"]], xi = allpars[["xi"]], dinf = allpars[["dinf"]], ch = allpars[["ch"]])

    odeout <- model.solve.ode$run(tt)
    sim <- as.data.frame(odeout)
    # browser()
    sim[sim<0] <- 0
    
    # Add observation uncertainty to simulated trajectories (in log10)
    sim$Vinf_noise <- log10(sim$Vinf) + rnorm(length(sim$Vinf), mean = 0, sd = 1)
    sim$Vtot_noise <- log10(sim$Vtot) + rnorm(length(sim$Vtot), mean = 0, sd = 1)
    
    sim$id <- i
    env <- rbind(env, sim)
  }
  summary_fit_info <- env %>% dplyr::group_by(t) %>% dplyr::summarise(Vinf_med = median(Vinf, na.rm = T),
                                                                Vtot_med = median(Vtot, na.rm = T),
                                                                Vinf_post_upr = quantile(Vinf, 0.975, na.rm = T),
                                                                Vinf_post_lwr = quantile(Vinf, 0.025, na.rm = T),
                                                                Vtot_post_upr = quantile(Vtot, 0.975, na.rm = T),
                                                                Vtot_post_lwr = quantile(Vtot, 0.025, na.rm = T),
                                                                Vinf_sampl_upr = quantile(Vinf_noise, 0.975, na.rm = T),
                                                                Vinf_sampl_lwr = quantile(Vinf_noise, 0.025, na.rm = T),
                                                                Vtot_sampl_upr = quantile(Vtot_noise, 0.975, na.rm = T),
                                                                Vtot_sampl_lwr = quantile(Vtot_noise, 0.025, na.rm = T))
  return(summary_fit_info)
}

plot_fit_with_PI <- function(summary_fit_info,
                             data, title = "",
                             xlim = NULL, ylim = NULL, legend = FALSE,
                             xlab = TRUE, ylab = TRUE, breaks = NULL, break_labels = NULL){
  
  p <- ggplot() + geom_hline(aes(yintercept = LOD.RNA), alpha = 0.5) +
    geom_hline(aes(yintercept = LOD.TCID), alpha = 0.5) +
    annotate(geom="text", x=12.5, y=2.2, label="LOD", cex = 7, alpha = 0.8) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_med)-1.96, ymax = log10(Vinf_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_med)-1.96, ymax = log10(Vtot_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_post_lwr), ymax = log10(Vinf_post_upr)), color = "NA", fill = "#00A3A6", alpha = 0.2) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_post_lwr), ymax = log10(Vtot_post_upr)), color = "NA", fill = "#bc80bd", alpha = 0.2) +
    # geom_ribbon(data = summary_fit_info, aes(x = t, ymin = Vinf_sampl_lwr, ymax = Vinf_sampl_upr), color = "NA", fill = "lightgrey", alpha = 0.6) +
    # geom_ribbon(data = summary_fit_info, aes(x = t, ymin = Vtot_sampl_lwr, ymax = Vtot_sampl_upr), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_point(data = data, aes(x = xvals, y = outcomeTCID, color = "TCID50")) + 
    geom_point(data = data, aes(x = xvals, y = outcomeRNA, color = "RNA")) +
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vinf_med), color = "TCID50")) + 
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vtot_med), color = "RNA")) +
    labs(y = "Viral load (log10/ml)",
         x = "days post infection",
         color = "") +
    scale_color_manual(values = c("TCID50" = "#00A3A6", "RNA" = "#bc80bd"),
                       guide = ifelse(legend,"legend","none")) + 
    ggtitle(title) +
    theme_classic() + theme(axis.text = element_text(size = 23),
                            axis.title = element_text(size = 24),
                            plot.title = element_text(size = 24),
                            legend.title = element_blank(),
                            legend.text = element_text(size = 20),
                            legend.position = c(0.85,0.6),
                            legend.box = "vertical")
  if(!is.null(breaks)){
    p <- p + scale_x_continuous(breaks = breaks,
                                labels = break_labels) +
      scale_y_continuous(breaks = seq(0,ylim[2],2))
  }
  if(!is.null(xlim) || !is.null(ylim)){
    p <- p + coord_cartesian(xlim = xlim, ylim = ylim, expand = FALSE)
  }
  if(!xlab){
    p <- p + theme(axis.title.x = element_blank())
  }
  if(!ylab){
    p <- p + theme(axis.title.y = element_blank())
  }
  return(p)
}

plot_fit_with_PI_tiff <- function(summary_fit_info,
                             data, title = "",
                             xlim = NULL, ylim = NULL, legend = FALSE,
                             xlab = TRUE, ylab = TRUE, breaks = NULL, break_labels = NULL){
  
  p <- ggplot() + geom_hline(aes(yintercept = LOD.RNA), alpha = 0.5, size = 0.25) +
    geom_hline(aes(yintercept = LOD.TCID), alpha = 0.5, size = 0.25) +
    annotate(geom="text", x=12.5, y=2.2, label="LOD", cex = 3.5, alpha = 0.8) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_med)-1.96, ymax = log10(Vinf_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_med)-1.96, ymax = log10(Vtot_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_post_lwr), ymax = log10(Vinf_post_upr)), color = "NA", fill = "#00A3A6", alpha = 0.2) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_post_lwr), ymax = log10(Vtot_post_upr)), color = "NA", fill = "#bc80bd", alpha = 0.2) +
    # geom_ribbon(data = summary_fit_info, aes(x = t, ymin = Vinf_sampl_lwr, ymax = Vinf_sampl_upr), color = "NA", fill = "lightgrey", alpha = 0.6) +
    # geom_ribbon(data = summary_fit_info, aes(x = t, ymin = Vtot_sampl_lwr, ymax = Vtot_sampl_upr), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_point(data = data, aes(x = xvals, y = outcomeTCID, color = "TCID50"), size = 0.8, alpha = 0.6) + 
    geom_point(data = data, aes(x = xvals, y = outcomeRNA, color = "RNA"), size = 0.8, alpha = 0.6) +
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vinf_med), color = "TCID50")) + 
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vtot_med), color = "RNA")) +
    labs(y = "Viral load (log10/ml)",
         x = "days post infection",
         color = "") +
    scale_color_manual(values = c("TCID50" = "#00A3A6", "RNA" = "#bc80bd"),
                       guide = ifelse(legend,"legend","none")) + 
    ggtitle(title) +
    theme_classic() + theme(axis.text.y = element_text(size = 12),
                            axis.text.x = element_text(size = 12),
                            axis.title = element_text(size = 15),
                            plot.title = element_text(size = 15),
                            legend.title = element_blank(),
                            legend.text = element_text(size = 11),
                            legend.position = c(0.85,0.6),
                            legend.box = "vertical")
  if(!is.null(breaks)){
    p <- p + scale_x_continuous(breaks = breaks,
                                labels = break_labels) +
      scale_y_continuous(breaks = seq(0,ylim[2],2))
  }
  if(!is.null(xlim) || !is.null(ylim)){
    p <- p + coord_cartesian(xlim = xlim, ylim = ylim, expand = FALSE)
  }
  if(!xlab){
    p <- p + theme(axis.title.x = element_blank())
  }
  if(!ylab){
    p <- p + theme(axis.title.y = element_blank())
  }
  return(p)
}


plot_fit_with_PI_dead_aspect <- function(summary_fit_info, data_dead, title = "",
                                         xlim = NULL, ylim = NULL, legend = FALSE,
                                         xlab = TRUE, ylab = TRUE, breaks = NULL, break_labels = NULL){
  
  p <- ggplot() + geom_hline(aes(yintercept = LOD.RNA), alpha = 0.5) +
    geom_hline(aes(yintercept = LOD.TCID), alpha = 0.5) +
    annotate(geom="text", x=12.5, y=2.2, label="LOD", cex = 7, alpha = 0.8) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_med)-1.96, ymax = log10(Vinf_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_med)-1.96, ymax = log10(Vtot_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_post_lwr), ymax = log10(Vinf_post_upr)), color = "NA", fill = "#00A3A6", alpha = 0.2) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_post_lwr), ymax = log10(Vtot_post_upr)), color = "NA", fill = "#bc80bd", alpha = 0.2) +
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vinf_med), color = "TCID50")) + 
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vtot_med), color = "RNA")) +
    geom_point(data = data_dead, aes(x = xvals, y = outcomeTCID, color = ifelse(time_of_death == F, "TCID50", "black"),
                                     shape = ifelse(time_of_death == F, "full_circle", "circle_contour"),
                                     fill = ifelse(time_of_death == F, NA, "TCID50")),
               size = 2, alpha = 0.7) + 
    geom_point(data = data_dead, aes(x = xvals, y = outcomeRNA, color = ifelse(time_of_death == F, "RNA", "black"),
                                     shape = ifelse(time_of_death == F, "full_circle", "circle_contour"),
                                     fill = ifelse(time_of_death == F, NA, "RNA")),
               size = 2, alpha = 0.7) +    
    labs(y = "Viral load (log10/ml)",
         x = "days post infection",
         color = "") +
    scale_color_manual(values = c("TCID50" = "#00A3A6", "RNA" = "#bc80bd",
                                  "black" = "black"), guide = ifelse(legend,"legend","none")) + 
    scale_shape_manual(values = c("full_circle" = 16, "circle_contour" = 21), guide = "none") +
    scale_fill_manual(values = c("TCID50" = "#00A3A6", "RNA" = "#bc80bd"), guide = "none") +
    ggtitle(title) +
    theme_classic() + theme(axis.text = element_text(size = 23),
                            axis.title = element_text(size = 24),
                            plot.title = element_text(size = 24),
                            legend.title = element_blank(),
                            legend.text = element_text(size = 20),
                            legend.position = c(0.85,0.6),
                            legend.box = "vertical")
  if(!is.null(breaks)){
    p <- p + scale_x_continuous(breaks = breaks,
                                labels = break_labels) +
      scale_y_continuous(breaks = seq(0,ylim[2],2))
  }
  if(!is.null(xlim) || !is.null(ylim)){
    p <- p + coord_cartesian(xlim = xlim, ylim = ylim, expand = FALSE)
  }
  if(!xlab){
    p <- p + theme(axis.title.x = element_blank())
  }
  if(!ylab){
    p <- p + theme(axis.title.y = element_blank())
  }
  return(p)
  
}

plot_fit_with_PI_dead_aspect_tiff <- function(summary_fit_info, data_dead, title = "",
                                         xlim = NULL, ylim = NULL, legend = FALSE,
                                         xlab = TRUE, ylab = TRUE, breaks = NULL, break_labels = NULL){
  
  p <- ggplot() + geom_hline(aes(yintercept = LOD.RNA), alpha = 0.5, size = 0.25) +
    geom_hline(aes(yintercept = LOD.TCID), alpha = 0.5, size = 0.25) +
    annotate(geom="text", x=12.5, y=2.2, label="LOD", cex = 3.5, alpha = 0.8) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_med)-1.96, ymax = log10(Vinf_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_med)-1.96, ymax = log10(Vtot_med)+1.96), color = "NA", fill = "lightgrey", alpha = 0.6) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vinf_post_lwr), ymax = log10(Vinf_post_upr)), color = "NA", fill = "#00A3A6", alpha = 0.2) +
    geom_ribbon(data = summary_fit_info, aes(x = t, ymin = log10(Vtot_post_lwr), ymax = log10(Vtot_post_upr)), color = "NA", fill = "#bc80bd", alpha = 0.2) +
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vinf_med), color = "TCID50")) + 
    geom_line(data = summary_fit_info, aes(x = t, y = log10(Vtot_med), color = "RNA")) +
    geom_point(data = data_dead, aes(x = xvals, y = outcomeTCID, color = ifelse(time_of_death == F, "TCID50", "black"),
                                     shape = ifelse(time_of_death == F, "full_circle", "circle_contour"),
                                     fill = ifelse(time_of_death == F, NA, "TCID50")),
               size = 0.8, alpha = 0.6) + 
    geom_point(data = data_dead, aes(x = xvals, y = outcomeRNA, color = ifelse(time_of_death == F, "RNA", "black"),
                                     shape = ifelse(time_of_death == F, "full_circle", "circle_contour"),
                                     fill = ifelse(time_of_death == F, NA, "RNA")),
               size = 0.8, alpha = 0.6) +    
    labs(y = "Viral load (log10/ml)",
         x = "days post infection",
         color = "") +
    scale_color_manual(values = c("TCID50" = "#00A3A6", "RNA" = "#bc80bd",
                                  "black" = "black"), guide = ifelse(legend,"legend","none")) + 
    scale_shape_manual(values = c("full_circle" = 16, "circle_contour" = 21), guide = "none") +
    scale_fill_manual(values = c("TCID50" = "#00A3A6", "RNA" = "#bc80bd"), guide = "none") +
    ggtitle(title) +
    theme_classic() + theme(axis.text.y = element_text(size = 12),
                            axis.text.x = element_text(size = 12),
                            axis.title = element_text(size = 15),
                            plot.title = element_text(size = 15),
                            legend.title = element_blank(),
                            legend.text = element_text(size = 11),
                            legend.position = c(0.85,0.6),
                            legend.box = "vertical")
  if(!is.null(breaks)){
    p <- p + scale_x_continuous(breaks = breaks,
                                labels = break_labels) +
      scale_y_continuous(breaks = seq(0,ylim[2],2))
  }
  if(!is.null(xlim) || !is.null(ylim)){
    p <- p + coord_cartesian(xlim = xlim, ylim = ylim, expand = FALSE)
  }
  if(!xlab){
    p <- p + theme(axis.title.x = element_blank())
  }
  if(!ylab){
    p <- p + theme(axis.title.y = element_blank())
  }
  return(p)
  
}


n_traj = 1000 # tried 10000 but the time it takes is unreasonable

summary_fit_info_lamb_surv <- compute_fit_and_uncertainty(results_mcmc = res.lamb.surv,
                                                      data = data.lamb.surv,
                                                      init.state = init.state.lamb.surv,
                                                      burnIn = burnIn,
                                                      model.solve.ode = simulate_basic_model_petrie,
                                                      n_traj = n_traj)
summary_fit_info_lamb_dead <- compute_fit_and_uncertainty(results_mcmc = res.lamb.dead,
                                                          data = data.lamb.dead,
                                                          init.state = init.state.lamb.dead,
                                                          burnIn = burnIn,
                                                          model.solve.ode = simulate_basic_model_petrie,
                                                          n_traj = n_traj)
summary_fit_info_calve <- compute_fit_and_uncertainty(results_mcmc = res.calve,
                                                          data = data.calve,
                                                          init.state = init.state.calve,
                                                          burnIn = burnIn,
                                                          model.solve.ode = simulate_basic_model_petrie,
                                                          n_traj = n_traj)
summary_fit_info_goat <- compute_fit_and_uncertainty(results_mcmc = res.goat,
                                                          data = data.goat,
                                                          init.state = init.state.goat,
                                                          burnIn = burnIn,
                                                          model.solve.ode = simulate_basic_model_petrie,
                                                          n_traj = n_traj)


p.lamb.surv <- plot_fit_with_PI_tiff(summary_fit_info_lamb_surv,
                                data.lamb.surv, 
                                title = "Lambs - survived (n=6)",
                                ylim = c(0,11.2), xlim = c(0,14.5), ylab = T, xlab = T, legend = F,
                                breaks = seq(0,14,1),
                                break_labels = c("0","","2","","4","","6","",
                                                 "8","","10","","12","","14"))

p.lamb.dead <- plot_fit_with_PI_dead_aspect_tiff(summary_fit_info_lamb_dead, data.lamb.dead,
                                            title = "Lambs - died (n=10)", 
                                            ylim = c(0,11.2), xlim = c(0,14.5), ylab = F, xlab = T, legend = F,
                                            breaks = seq(0,14,1),
                                            break_labels = c("0","","2","","4","","6","",
                                                             "8","","10","","12","","14"))
p.goat <- plot_fit_with_PI_tiff(summary_fit_info_goat, data.goat, 
                           title = "Goats (n=8)", 
                           ylim = c(0,11.2), xlim = c(0,14.5), ylab = T, xlab = F, legend = T,
                           breaks = seq(0,14,1),
                           break_labels = c("0","","2","","4","","6","",
                                            "8","","10","","12","","14"))

p.calve <- plot_fit_with_PI_tiff(summary_fit_info_calve, data.calve, 
                            title = "Calves (n=8)",
                            ylim = c(0,11.2), xlim = c(0,14.5), ylab = F, xlab = F, legend = F,
                            breaks = seq(0,14,1),
                            break_labels = c("0","","2","","4","","6","",
                                             "8","","10","","12","","14"))


P <- (p.goat|p.calve)/(p.lamb.surv | p.lamb.dead)
# png("../Output/figures/fits_prediction_interval_two_envelopes.png", width = 900, height = 900)
# print(P)
# dev.off()
tiff(filename = "../../paper/figures/final_formatting/Fig2.tiff", width = 1800, height = 1800, res = 300)
plot(P)
# plot(p.goat|p.calve)
dev.off()

