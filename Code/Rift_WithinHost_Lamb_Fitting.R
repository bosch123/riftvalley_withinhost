## ---------------------------
##
## Script name: 
##
## Purpose of script:
##
## Author: Helene Cecilia
##
## Date Created: 2021-02-23

rm(list=ls())

## Loading Packages  ------------------
library(tictoc)
library(ggplot2)
library(BayesianTools)
library(gtools) # for combinations
library(coda) # to convert to mcmc before using convertCoda
library(stringr) # for str_replace
library(dplyr)
library(inauguration)
library(HDInterval)
library(gghdr)

## Set Work Directory ------------------------------------------------------
setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) # set to source file location 
getwd()
options(error = browser)
## Load source files ------------------
source('Rift_WithinHost_Functions_Build_BasicModel.R')
source("Rift_WithinHost_Functions_MCMC.R")

load("../Output/global_params.RData")
`%notin%` <- Negate(`%in%`)
LOD.RNA <- 1.7 
## -------------------------------

data <- read.csv("../Data/speciesData/species_data.csv")
names(data) <- c("xvals", "outcomeRNA", "outcomeTCID", "host_type", "outcome_survival", "id", "study")
data <- data[!(is.na(data$outcomeRNA) & is.na(data$outcomeTCID)),]

lamb <- data[data$host_type == "lamb",]
lamb.surv <- data[data$host_type == "lamb" & data$outcome_survival == "surv",]
lamb.dead <- data[data$host_type == "lamb" & data$outcome_survival == "die",] 

## -------------------------------

# FIXED PARAM GO IN INIT.STATE
# U0 (and beta) are fixed within the MCMC but their value is assessed through likelihood profiles.
# The MCMC is therefore run for each of these sets of fixed parameters, and we select the one yielding the maximum likelihood
target <- seq(4,6.5,0.5)
init.state1 <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[1], beta = 48/(10**target[1]))
init.state2 <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[2], beta = 48/(10**target[2]))
init.state3 <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[3], beta = 48/(10**target[3]))
init.state4 <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[4], beta = 48/(10**target[4]))
init.state5 <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[5], beta = 48/(10**target[5]))
init.state6 <- c(Vinf0 = 52.6, kl = 3, U0 = 10**target[6], beta = 48/(10**target[6]))

# ESTIMATED PARAM GO IN INIT.PARAM
#Chain 1
init.param <- c(delta = 1.5, p = 20, xi = 45, dinf = 2, ch = 4)
#Chain 2
init.param2 <- c(delta = 0.5, p = 200, xi = 500, dinf = 4, ch = 2)
#Chain 3
init.param3 <- c(delta = 2.5, p = 2000, xi = 5, dinf = 1, ch = 6)

# PRIORS ARE GIVEN FOR ESTIMATED PARAMS
parmin = c(delta = 0.1, p = 0.2, xi = 1, dinf = 0.1, ch = 0.1)
parmax = c(delta = 10, p = 3e4, xi = 1000, dinf = 10, ch = 10)

# Done externally : Optimized standard deviations for sampling of each parameter, for each set of fixed parameters
# sdProposal.list.all <- list(c(delta = 0.19, p = 10.08, xi = 568.78, dinf = 3.81, ch = 0.58),
#                             c(delta = 0.19, p = 10.08, xi = 568.78, dinf = 3.81, ch = 0.58),
#                             c(delta = 0.19, p = 10.08, xi = 568.78, dinf = 3.81, ch = 0.58),
#                             c(delta = 0.53, p = 8.83, xi = 451.15, dinf = 2.04, ch = 0.44),
#                             c(delta = 0.79, p = 8.97, xi = 382.75, dinf = 1.64, ch = 0.40),
#                             c(delta = 0.66, p = 11.76, xi = 179.40, dinf = 1.17, ch = 0.39))
# 
# sdProposal.list.surv <- list(c(delta = 0.30, p = 25.27, xi = 70.12, dinf = 17.79, ch = 0.56),
#                              c(delta = 0.30, p = 25.27, xi = 70.12, dinf = 17.79, ch = 0.56),
#                              c(delta = 0.76, p = 17.83, xi = 121.40, dinf = 10.36, ch = 0.51),
#                              c(delta = 1.10, p = 16.84, xi = 63.01, dinf = 7.15, ch = 0.52),
#                              c(delta = 2.02, p = 14.50, xi = 88.01, dinf = 8.77, ch = 0.56),
#                              c(delta = 1.46, p = 18.59, xi = 21.12, dinf = 5.24, ch = 0.51))
# 
# sdProposal.list.dead <- list(c(delta = 0.15, p = 17.16, xi = 951.30, dinf = 9.15, ch = 2.75),
#                              c(delta = 0.15, p = 17.16, xi = 951.30, dinf = 9.15, ch = 2.75),
#                              c(delta = 0.15, p = 17.16, xi = 951.30, dinf = 9.15, ch = 2.75),
#                              c(delta = 0.13, p = 53.81, xi = 230.30, dinf = 18.12, ch = 2.73),
#                              c(delta = 0.88, p = 19.97, xi = 458.00, dinf = 3.37, ch = 1.07),
#                              c(delta = 1.26, p = 12.31, xi = 553.26, dinf = 2.58, ch = 1.01))

# init.list <- list(init.state1,
#                   init.state2,
#                   init.state3,
#                   init.state4,
#                   init.state5,
#                   init.state6)
# index <- c(1,2,3,4,5,6)

# Run ----- Done on a cluster
# for(i in seq(length(init.list))){
#   init.state <- init.list[[i]]
#   results_opti <- get.optimal.sdProposal(init.param = init.param, finetune.Iterations = nbIter, burnIn = burnIn,
#                                          ft.sdProposals = c(delta = 0.53, p = 8.83, xi = 451.15, dinf = 2.04, ch = 0.44), # or sdProposal.list.all[[i]]
#                                          model.solve.ode = simulate_basic_model_petrie,
#                                          parmin = parmin, parmax = parmax, 
#                                          dt = 0.1,
#                                          likelihood.function = model.point.likelihood,
#                                          init.state = init.state, data = lamb)
#   saveRDS(results_opti, file = paste("../Output/speciesFitting/LONG_lamb_all_combi",index[i],".rds", sep=""))
#   sdProposal <- results_opti$sd # keeps the optimized standard deviations as a starting point for next set of parameters
#   # comment if you already provide a list of optimized sampling standard deviations
# }
# 
# for(i in seq(length(init.list))){
#   init.state <- init.list[[i]]
#   results_opti <- get.optimal.sdProposal(init.param = init.param, finetune.Iterations = nbIter, burnIn = burnIn,
#                                          ft.sdProposals = c(delta = 1.10, p = 16.84, xi = 63.01, dinf = 7.15, ch = 0.52), # or sdProposal.list.surv[[i]]
#                                          model.solve.ode = simulate_basic_model_petrie,
#                                          parmin = parmin, parmax = parmax,
#                                          dt = 0.1,
#                                          likelihood.function = model.point.likelihood,
#                                          init.state = init.state, data = lamb.surv)
#   saveRDS(results_opti, file = paste("../Output/speciesFitting/LONG_lamb_surv_combi",index[i],".rds", sep=""))
#   sdProposal <- results_opti$sd # keeps the optimized standard deviations as a starting point for next set of parameters
#   # comment if you already provide a list of optimized sampling standard deviations
# }
# 
# for(i in seq(length(init.list))){
#   init.state <- init.list[[i]]
#   results_opti <- get.optimal.sdProposal(init.param = init.param, finetune.Iterations = nbIter, burnIn = burnIn,
#                                          ft.sdProposals = c(delta = 0.20, p = 36.83, xi = 350.89, dinf = 12.21, ch = 2.24), # or sdProposal.list.dead[[i]]
#                                          model.solve.ode = simulate_basic_model_petrie,
#                                          parmin = parmin, parmax = parmax, 
#                                          dt = 0.1,
#                                          likelihood.function = model.point.likelihood,
#                                          init.state = init.state, data = lamb.dead)
#   saveRDS(results_opti, file = paste("../Output/speciesFitting/LONG_lamb_dead_combi",index[i],".rds", sep=""))
#   sdProposal <- results_opti$sd # keeps the optimized standard deviations as a starting point for next set of parameters
#   # comment if you already provide a list of optimized sampling standard deviations
# }

# Common convergence and joint posteriors ----
# Done externally : select combination of parameters yielding the best likelihood (see file name)
# for each group (surviving lambs, dying lambs, all lambs)
# chain1 <- readRDS("../Output/speciesFitting/LONG_lamb_all_combi4.rds")
# chain2 <- readRDS("../Output/speciesFitting/LONG_lamb_all_combi4_chain2.rds")
# chain3 <- readRDS("../Output/speciesFitting/LONG_lamb_all_combi4_chain3.rds")
# chain1 <- readRDS("../Output/speciesFitting/LONG_lamb_surv_combi4.rds")
# chain2 <- readRDS("../Output/speciesFitting/LONG_lamb_surv_combi4_chain2.rds")
# chain3 <- readRDS("../Output/speciesFitting/LONG_lamb_surv_combi4_chain3.rds")
chain1 <- readRDS("../Output/speciesFitting/LONG_lamb_dead_combi5.rds")
chain2 <- readRDS("../Output/speciesFitting/LONG_lamb_dead_combi5_chain2.rds")
chain3 <- readRDS("../Output/speciesFitting/LONG_lamb_dead_combi5_chain3.rds")

# We found that lambs surviving and dying were best fitted with different initial numbers of target cells
# init.state <- init.state4
init.state <- init.state5

# res1.mcmc <- mcmc(chain1$mcmc$parameters)
# res2.mcmc <- mcmc(chain2$mcmc$parameters)
# res3.mcmc <- mcmc(chain3$mcmc$parameters)
# 
# lamb.list <- mcmc.list(res1.mcmc, res2.mcmc, res3.mcmc)
# traceplot(lamb.list) # chose relevant burn-in

burnIn <- 20000
nbIter <- 100000
res1.mcmc <- mcmc(chain1$mcmc$parameters[burnIn:nbIter,])
res2.mcmc <- mcmc(chain2$mcmc$parameters[burnIn:nbIter,])
res3.mcmc <- mcmc(chain3$mcmc$parameters[burnIn:nbIter,])

lamb.list <- mcmc.list(res1.mcmc, res2.mcmc, res3.mcmc)
gelman.diag(lamb.list)
gelmanDiagnostics(lamb.list, plot = T) # should be < 1.1
effectiveSize(lamb.list) 

df1 <- data.frame(iter = rep(seq(1,nbIter),nbParam), paramValue = as.vector(chain1$mcmc$parameters),
                  paramName = rep(names(init.param), each = nbIter),
                  logLik = rep(chain1$mcmc$logLik, nbParam), chain = 1)
df2 <- data.frame(iter = rep(seq(1,nbIter),nbParam), paramValue = as.vector(chain2$mcmc$parameters),
                  paramName = rep(names(init.param), each = nbIter),
                  logLik = rep(chain2$mcmc$logLik, nbParam), chain = 2)
df3 <- data.frame(iter = rep(seq(1,nbIter),nbParam), paramValue = as.vector(chain3$mcmc$parameters),
                  paramName = rep(names(init.param), each = nbIter),
                  logLik = rep(chain3$mcmc$logLik, nbParam), chain = 3)
df <- rbind(df1,df2,df3)

p <- ggplot(df[df$iter > burnIn,]) + geom_line(aes(x = iter, y = paramValue, group = chain, color = as.factor(chain)), alpha = 0.5) +
  facet_wrap(vars(paramName), scales = "free", shrink = T) + theme_classic() + 
  guides(color = guide_legend(override.aes = list(alpha = 1, linewidth = 2))) +
  scale_x_continuous(breaks = c(20000,60000,100000), limits = c(20000,105000)) +
  labs(color = "Chain") + ggtitle("All lambs") + 
  theme(strip.text.x = element_text(size = 21),
        axis.text.y = element_text(size = 20),
        axis.text.x = element_text(size = 19),
        axis.title = element_text(size = 20),
        title = element_text(size = 22),
        legend.title = element_text(size = 21),
        legend.text = element_text(size = 20),
        legend.position = c(0.75,0.25))
png(filename = "../Output/figures/lamb_all_trace_multi_chain.png", width = 850, height = 500)
print(p)
dev.off()

# p <- ggplot(df[df$iter>=burnIn,]) + geom_histogram(aes(x = paramValue, y = ..density..),color = "black", fill = "white") +
#   geom_density(aes(x = paramValue), alpha = .2, fill = "#FF6666") +
#   facet_wrap(vars(paramName), scales = "free", shrink = T) + theme_classic()
# print(p)

df.chain1 <- as.data.frame(chain1$mcmc)
df.chain1$iter <- seq(nbIter)
df.chain2 <- as.data.frame(chain2$mcmc)
df.chain2$iter <- seq(nbIter)
df.chain3 <- as.data.frame(chain3$mcmc)
df.chain3$iter <- seq(nbIter)
df.chains <- rbind(df.chain1, df.chain2, df.chain3)
df.chains <- df.chains[,c("logLik","iter","parameters.delta",
                          "parameters.p","parameters.xi",
                          "parameters.dinf","parameters.ch")]
names(df.chains) <- c("logLik","iter","delta",
                      "p","xi",
                      "dinf","ch")
# test <- calc.dic.joint(df.chains, nbIter, burnIn, fixed_param = init.state,
#                  model = simulate_basic_model_petrie, data = data.surv, lik.fun = logLik.trajectory)
write.csv(df.chains, file = "../Output/speciesFitting/joint_lamb_all.csv", row.names = F)

